﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;

namespace CountdownWordGame
{
	public class WordGenerator
	{
		public string[] Wordlist
		{ get; private set; }

		public IEnumerable<string> GenerateWords(IEnumerable<char> letters, int length)
		{
			if (length > 0)
			{
				foreach (char item in letters)
				{
					foreach (String suffix in GenerateWords(letters, length - 1))
					{
						yield return item + suffix;
					}
				}
			}
			else
			{
				yield return string.Empty;
			}
		}
	}
}
