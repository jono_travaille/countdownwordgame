﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Timers;

namespace CountdownWordGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rng = new Random();
            char[] vowelArray = { 'a', 'e', 'i', 'o', 'u' };
            char[] consonantArray = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };
            List<char> playerLetterArray = new List<char>();
            List<string> playerWordArray = new List<string>();


            System.Timers.Timer gameTimer = new System.Timers.Timer();
            gameTimer.Elapsed += new System.Timers.ElapsedEventHandler(endRound);
            gameTimer.Interval = 30000;


            Console.WriteLine("Welcome to single player Countdown. \n" +
                "To begin, please specify whether you would like a consonant (c), or a vowel (v):");
            do
            {
                int randomInt;
                Console.WriteLine("Enter \"c\" or \"v\"");
                string userInput = Console.ReadLine();

                if (userInput.ToLower() == "v")
                {
                    randomInt = rng.Next(0, 4);
                    playerLetterArray.Add(vowelArray[randomInt]);
                }
                else if (userInput.ToLower() == "c")
                {
                    randomInt = rng.Next(0, 20);
                    playerLetterArray.Add(consonantArray[randomInt]);
                }
                else
                {
                    Console.WriteLine("Please enter either \"c\" or \"v\" to select a consonant or a vowel respectively");
                }

                Console.WriteLine("Current letter selection: {0}", string.Join(", ", playerLetterArray));

            } while (playerLetterArray.Count < 9);

            
            Console.WriteLine("You now have to create the longest word you can with the letters you have been given. \n" +
                "Your letters are: {0}", string.Join(", ", playerLetterArray));

            gameTimer.Enabled = true;
            do
            {
                Console.WriteLine("Enter a word: ");
                string userInput = Console.ReadLine();
                int validity = IsLegal(userInput, playerLetterArray);

                switch (validity)
                {
                    case 0:
                        Console.WriteLine("Word submitted: " + userInput);
                        break;
                    case 1:
                        Console.WriteLine("You must use only the letters provided in your current letter selection. Each letter can only be used once.");
                        break;

                }
                //word not legal according to dictionary

                //Console.WriteLine("That is not a valid word. Only words that appear in the dictionary are valid, except proper nouns, and hyphenated terms.");

            } while (gameTimer.Enabled);




            void endRound(object source, ElapsedEventArgs args)
            {
                gameTimer.Enabled = false;
            }
        }

        public static int IsLegal(string input, List<char> letters)
        {
            List<char> arrayCopy = new List<char>(letters);

            foreach (char c in input.ToCharArray())
            {
                if (!arrayCopy.Contains(c))
                {
                    return 1;  //Letter not in current selection
                }
                arrayCopy.Remove(c); //To avoid a letter being used more than once...
            }

            return 0; //Valid word
        }
    }
}
